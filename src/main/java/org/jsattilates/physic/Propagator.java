package org.jsattilates.physic;

import org.jsattilates.math.Point3D;
import org.jsattilates.spaceobject.SpaceObject;

import java.time.LocalDateTime;
import java.util.Map;

/**
 * Интерфейс для расчета позиции космического объекта и его трасс.
 */
public interface Propagator {
    /**
     * @param object космический объект
     * @param dateTime момент времени для которого необходимо определить позицию объекта
     * @return позиция объекта в указанный момент времени
     */
    Point3D getPosition(SpaceObject object, LocalDateTime dateTime);

    /**
     * Построение трассы движения космического объекта в промежутке
     * между начальным и конечным моментом времени с указанным шагом.
     * @param object космический объект
     * @param upDateTime начальный момент времени
     * @param toDateTime конечный момент времени
     * @param step шаг в минутах
     * @return трасса движения космического объекта
     */
    Map<LocalDateTime, Point3D> getTrace(SpaceObject object,
                                         LocalDateTime upDateTime,
                                         LocalDateTime toDateTime,
                                         long step);
}
