package org.jsattilates.physic;

import org.jsattilates.math.Point3D;
import org.jsattilates.math.TimeUtils;
import org.jsattilates.spaceobject.SpaceObject;
import sgp4v.ObjectDecayed;
import sgp4v.SatElsetException;
import sgp4v.Sgp4Data;
import sgp4v.Sgp4Unit;

import javax.vecmath.Vector3d;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

/**
 * Класс для предсказания позиций космического объекта по SGP4 модели.
 * Для расчета позиций по TLE использован модуль sgp4v: https://celestrak.com/software/tskelso-sw.asp
 */
public class Sgp4Propagator implements Propagator {

    private static final double EARTH_RADIUS = 6378.135;   // Радиус земли в километрах


    public Point3D getPosition(final SpaceObject object, final LocalDateTime dateTime) {
        int plusValue = 1;  // Чтобы начальная и конечная даты отличались
        int step = 20;      // Когда шаг больше разницы дат - будет возвращено
                            // одно значение, соответствующие начальной дате
        return (Point3D)getTrace(object, dateTime,
                dateTime.plusMinutes(plusValue), step).values().toArray()[0];
    }


    public Map<LocalDateTime, Point3D> getTrace(final SpaceObject object,
                                                final LocalDateTime upDateTime,
                                                final LocalDateTime toDateTime,
                                                long step) {

        if (upDateTime.isAfter(toDateTime)) {
            throw new IllegalArgumentException("The given up datetime value " +
                    "must be less then the to datetime value.");
        } else if (step <= 0) {
            throw new IllegalArgumentException("The given value of step " +
                    "must be positive.");
        }

        Vector<Sgp4Data> results;
        try {
            Sgp4Unit sgp4 = new Sgp4Unit();
            results = sgp4.runSgp4(object.getTle().getFirstTleLine(),
                    object.getTle().getSecondTleLine(),
                    upDateTime.getYear(), TimeUtils.convertDateTimeToDayOfYear(upDateTime),
                    toDateTime.getYear(), TimeUtils.convertDateTimeToDayOfYear(toDateTime),
                    step);
        } catch (ObjectDecayed exc) {
            throw new IllegalArgumentException("The space object with given " +
                    "TLE has decayed orbit.");
        } catch (SatElsetException exc) {
            throw new IllegalArgumentException(exc.getMessage());
        }

        Map<LocalDateTime, Point3D> trace = new LinkedHashMap<LocalDateTime, Point3D>();

        for (int i = 0; i < results.size() - 1; i++) {
            Vector3d pos = results.get(i).getPosn();
            pos.scale(EARTH_RADIUS);
            trace.put(upDateTime.plusMinutes(step * i), new Point3D(pos));
        }

        return trace;
    }

}
