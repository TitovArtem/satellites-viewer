package org.jsattilates.mapdrawer;
import org.jsattilates.math.Coordinates;
import org.jsattilates.math.Point2D;


/**
 * Класс для первода координат из широты и долготы в двумерные координаты по проекции Меркатора.
 */
public class MercatorProjection implements MapProjection {

    /* Размеры карты в пискелях */
    private int width, height;

    public MercatorProjection(int width, int height) {
        if (width <= 0 || height <= 0) {
            throw new IllegalArgumentException("The given values of width and " +
                    "height must be positive.");
        }
        this.width = width;
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        if (width <= 0) {
            throw new IllegalArgumentException("The given values of width " +
                    "must be positive.");
        }
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        if (height <= 0) {
            throw new IllegalArgumentException("The given values of height " +
                    "must be positive.");
        }
        this.height = height;
    }

    public Point2D[] project(final Coordinates[] coordinates) {

        Point2D[] result = new Point2D[coordinates.length];

        for (int i = 0; i < coordinates.length; i++) {
            double lon = coordinates[i].longitude;
            double lat = coordinates[i].latitude;

            result[i] = new Point2D();
            result[i].x = width / 2.0 + (lon * 85.0 / 15.0);
            result[i].y = height / 2.0 - Math.toDegrees(Math.log(Math.tan((Math.PI / 4) +
                    Math.toRadians(lat) / 2.0))) * 85.0 / 15.0;
        }
        return result;
    }
}