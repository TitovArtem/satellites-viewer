package org.jsattilates.mapdrawer;
import javafx.scene.image.Image;
import org.jsattilates.math.Point2D;


/**
 * Интерфейс для отрисовки трассы спутника на карте.
 */
public interface MapDrawer {
    /**
     * Отрисовывает трассу движения объекта на заданном изображении.
     * @param map изображение
     * @param track массив точек для отрисовки трассы
     * @return изображение с отрисованной трассой */
    Image drawTrack(Image map, Point2D[] track);

    /**
     * Отрисовка позиции объекта на заданном изображении.
     * @param map изоражение
     * @param position позиция объекта
     * @return изображение с отрисованной меткой объекта */
    Image drawPosition(Image map, Point2D position);
}