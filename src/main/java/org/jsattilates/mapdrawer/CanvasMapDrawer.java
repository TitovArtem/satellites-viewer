package org.jsattilates.mapdrawer;
import javafx.scene.SnapshotParameters;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import org.jsattilates.math.Point2D;


/**
 * Реализация рендера на карте с помощью примитивов JavaFx.
 */
public class CanvasMapDrawer implements MapDrawer {

    private Color trackColor = Color.ORANGE;   // Цвет трассы
    private float tracksWidth = 8f;            // Толщина линий для отрисовки трасс
    private Image satelliteIcon;               // Изображение для отображения метки
    private int satelliteIconWidth;            // Ширина и высота изображения метки при
    private int satelliteIconHeight;           // отображении на карте в пискелях
    private String objectName;                 // Название объекта
    private Color objectNameColor;             // Цвет строки для названия объекта
    private int objectNameFontSize = 50;       // Размер шрифта для имени объекта


    public CanvasMapDrawer(Color trackColor, float tracksWidth, Image satelliteIcon) {
        if (trackColor == null) {
            throw new NullPointerException("The given color of tracks has null value.");
        }

        if (satelliteIcon == null) {
            throw new NullPointerException("The given image for the satellite" +
                    " icon has null value.");
        }

        this.trackColor = trackColor;
        this.tracksWidth = tracksWidth;
        this.satelliteIcon = satelliteIcon;
        objectNameColor = Color.WHITE;
    }

    public CanvasMapDrawer() {
        satelliteIcon = new Image("satellite-icon.png");
        satelliteIconHeight = satelliteIconWidth = 100;
        objectNameColor = Color.WHITE;
    }

    public Color getTrackColor() {
        return trackColor;
    }

    public void setTrackColor(Color trackColor) {
        if (trackColor == null) {
            throw new NullPointerException("The given color of tracks has null value.");
        }

        this.trackColor = trackColor;
    }

    public float getTracksWidth() {
        return tracksWidth;
    }

    public void setTracksWidth(float tracksWidth) {
        if (tracksWidth < 1) {
            throw new IllegalArgumentException("The given width of the " +
                    "tracks must be more then 1.");
        }
        this.tracksWidth = tracksWidth;
    }

    public Image getSatelliteIcon() {
        return satelliteIcon;
    }

    public void setSatelliteIcon(Image satelliteIcon) {
        if (satelliteIcon == null) {
            throw new NullPointerException("The given image for the satellite" +
                    " icon has null value.");
        }

        this.satelliteIcon = satelliteIcon;
    }

    public int getSatelliteIconWidth() {
        return satelliteIconWidth;
    }

    public void setSatelliteIconWidth(int satelliteIconWidth) {
        if (satelliteIconWidth <= 0) {
            throw new IllegalArgumentException("The given width of the " +
                    "satellite icon must be positive.");
        }
        this.satelliteIconWidth = satelliteIconWidth;
    }

    public int getSatelliteIconHeight() {
        return satelliteIconHeight;
    }

    public void setSatelliteIconHeight(int satelliteIconHeight) {
        if (satelliteIconHeight <= 0) {
            throw new IllegalArgumentException("The given height of the " +
                    "satellite icon must be positive.");
        }
        this.satelliteIconHeight = satelliteIconHeight;
    }

    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    public Color getObjectNameColor() {
        return objectNameColor;
    }

    public void setObjectNameColor(Color objectNameColor) {
        if (objectNameColor == null) {
            throw new NullPointerException("The given color of name has null value.");
        }
        this.objectNameColor = objectNameColor;
    }

    public int getObjectNameFontSize() {
        return objectNameFontSize;
    }

    public void setObjectNameFontSize(int objectNameFontSize) {
        if (objectNameFontSize <= 0) {
            throw new IllegalArgumentException("The given font size must be positive.");
        }
        this.objectNameFontSize = objectNameFontSize;
    }

    private boolean isRightDirectedTrack(final Point2D[] track) {
        if (track.length < 3) return true;

        // Три точки необходимы, т.к. может быть перенос через границу карты
        return (track[0].x <= track[1].x || track[1].x <= track[2].x);
    }

    private Point2D[] getReverseTrack(final Point2D[] track) {
        Point2D temp[] = new Point2D[track.length];
        for (int i = 0; i < track.length; i++) {
            temp[i] = track[track.length - i - 1];
        }
        return temp;
    }

    public Image drawTrack(Image map, Point2D[] track) {
        Canvas canvas = new Canvas(map.getWidth(), map.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(map, 0, 0);
        gc.setStroke(trackColor);
        gc.setLineWidth(tracksWidth);

        if (!isRightDirectedTrack(track)) track = getReverseTrack(track);

        for (int i = 1; i < track.length; i++) {
            Point2D from = track[i - 1], to = track[i];

            if (to.x < from.x) {
                gc.strokeLine(from.x, from.y, to.x + map.getWidth(), to.y);
                gc.strokeLine(from.x - map.getWidth(), from.y, to.x, to.y);
            }  else {
                gc.strokeLine(from.x, from.y, to.x, to.y);
            }
}
        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);
        return canvas.snapshot(params, null);
    }

    public Image drawPosition(final Image map, final Point2D position) {
        Canvas canvas = new Canvas(map.getWidth(), map.getHeight());
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.drawImage(map, 0, 0);

        gc.drawImage(satelliteIcon, position.x - satelliteIconWidth / 2,
                position.y - satelliteIconHeight / 2,
                satelliteIconWidth, satelliteIconHeight);

        if (objectName != null) {
            gc.setFill(objectNameColor);
            gc.setFont(new Font(objectNameFontSize));
            gc.fillText(objectName, position.x + objectNameFontSize / 4,
                    position.y - objectNameFontSize / 4);
        }
        SnapshotParameters params = new SnapshotParameters();
        params.setFill(Color.TRANSPARENT);
        return canvas.snapshot(params, null);
    }

}