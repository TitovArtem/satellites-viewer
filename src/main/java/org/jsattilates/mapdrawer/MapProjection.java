package org.jsattilates.mapdrawer;
import org.jsattilates.math.Coordinates;
import org.jsattilates.math.Point2D;

/**
 * Интерфейс для перевода долготы и широты в двумерные координаты по заднной проекции.
 */
public interface MapProjection {

    /**
     * Преобразует долготу и широту в двумерные координаты по заданной проекции.
     * @param coordinates массив точек заданных в виде долготы и широты
     * @return массив двумерных координат в заданной проекции
     */
    Point2D[] project(Coordinates[] coordinates);
}