package org.jsattilates.spaceobject;

import java.io.Serializable;

/**
 * Класс для зандания космических объектов.
 */
public class SpaceObject implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    private Tle tle;

    public SpaceObject(final Tle tle) {
        setTle(tle);
    }

    public SpaceObject(final String tle) {
        this.tle = new Tle(tle);
    }

    public Tle getTle() {
        return tle;
    }

    public void setTle(final Tle tle) {
        try {
            this.tle = (Tle) tle.clone();
        } catch (CloneNotSupportedException exc) {
            throw new IllegalStateException(exc.getMessage());
        }
    }

    public SatelliteOrbitType getOrbitType() {
        double e = tle.getEccentricity();
        double meanMotion = tle.getMeanMotion();
        if (e < 0.01 && (meanMotion >= 0.99 && meanMotion <= 1.01)) {
            return SatelliteOrbitType.GEO;
        } else if ((600 <= getPeriod() && getPeriod() <= 800) && e < 0.25) {
            return SatelliteOrbitType.MEO;
        } else if (meanMotion > 11.25 && e < 0.25) {
            return SatelliteOrbitType.LEO;
        } else if (e > 0.25) {
            return SatelliteOrbitType.HEO;
        }
        return SatelliteOrbitType.UNDEFINED;
    }

    public double getPeriod() {
        return 1440.0 / tle.getMeanMotion();
    }

    public double getVelocity(double altitude) {
        return Math.sqrt(398600.5 / (6378.14 + altitude));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpaceObject that = (SpaceObject) o;

        return tle != null ? tle.equals(that.tle) : that.tle == null;

    }

    @Override
    public int hashCode() {
        return tle != null ? tle.hashCode() : 0;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        SpaceObject object = (SpaceObject)super.clone();
        object.tle = (Tle)object.tle.clone();
        return object;
    }

    @Override
    public String toString() {
        return "SpaceObject{" + tle.getSatelliteName() + '}';
    }
}