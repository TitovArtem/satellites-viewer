package org.jsattilates.spaceobject;

import java.io.Serializable;


/**
 * Описание полей TLE формата данных.
 */

public class Tle implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    /** Первая строка TLE. */
    private final String firstLine;
    /** Вторая строка TLE. */
    private final String secondLine;

    /** Имя объекта. */
    private final String name;
    /** Номер спутника в базе данных NORAD. */
    private final long satelliteNumber;
    /** Классификация спутника. */
    private final char category;
    /** Год запуска. */
    private final int launchYear;
    /** Номер запуска в этом году. */
    private final int launchNumber;
    /** Часть запуска. */
    private final String launchPiece;
    /** Год эпохи. */
    private final int epochYear;
    /** Время эпохи (целая часть номер дня в году, дробная - часть дня). */
    private final double epochDay;
    /** Первая производная от среднего движения, деленная на два. */
    private final double firstMeanMotionDerivative;
    /** Вторая призводная от среднего движения, деленная на шесть. */
    private final double secondMeanMotionDerivative;
    /** Коэффициент торможения B. */
    private final double dragTerm;
    /** Типы эфемерид (сейчас всегда ноль). */
    private final int ephemerisType = 0;
    /** Номер (версия) элемента. */
    private final int elementNumber;


    /** Наклонение в градусах. */
    private final double inclination;
    /** Долгота восходящего узла в градусах. */
    private final double ascendingNode;
    /** Эксцентриситет. */
    private final double eccentricity;
    /** Аргумент парицентра в градусах. */
    private final double perigeeArgument;
    /** Средняя аномалия в градусах. */
    private final double meanAnomaly;
    /** Частота обращения (оборотов в день). */
    private final double meanMotion;
    /** Номер витка на момент эпохи. */
    private final long revolutions;


    public Tle(final String tleString) {
        String[] tleElements = tleString.split("\n");
        if (tleElements.length > 3) {
            throw new IllegalArgumentException("The given TLE string can't " +
                    "has more then 3 rows.");
        } else if (tleElements.length < 2) {
            throw new IllegalArgumentException("The given TLE string must " +
                    "have 2 or 3 rows.");
        }
        
        if (tleElements.length == 2) {
            this.name = "";
            this.firstLine = tleElements[0];
            this.secondLine = tleElements[1];
        } else {
            this.name = tleElements[0];
            this.firstLine = tleElements[1];
            this.secondLine = tleElements[2];
        }

        try {
            if(firstLine.length() != 69 || secondLine.length() != 69)
                throw new StringIndexOutOfBoundsException("inccorect length of string!");
            satelliteNumber = parseLong(firstLine, 2, 7);
            category = firstLine.charAt(7);
            launchYear = parseInt(firstLine, 9, 11);
            launchNumber = parseInt(firstLine, 11, 14);
            launchPiece = firstLine.substring(14, 17).trim();
            epochYear = parseInt(firstLine, 18, 20);
            epochDay = parseDouble(firstLine, 20, 32);
            firstMeanMotionDerivative = parseDouble(firstLine, 33, 43);
            secondMeanMotionDerivative = Double.parseDouble((firstLine.substring(44, 45) + '.' +
                    firstLine.substring(45, 50) + 'e' +
                    firstLine.substring(50, 52)).replace(' ', '0'));
            dragTerm = Double.parseDouble((firstLine.substring(53, 54) + '.' +
                    firstLine.substring(54, 59) + 'e' +
                    firstLine.substring(59, 61)).replace(' ', '0'));
            elementNumber = parseInt(firstLine.replace(' ', '0'), 64, 68);
            int checkSum = parseInt(firstLine, 68, 69);
            if (checkSum > 9)
                throw new IllegalArgumentException("check sum  isn't corrected");

            Long satNumberOfTwoLine = parseLong(secondLine, 2, 7);
            if (satNumberOfTwoLine != satelliteNumber) {
                throw new IllegalArgumentException("The norad numbers aren't the same.");
            }
            inclination = parseDouble(secondLine, 8, 16);
            ascendingNode = parseDouble(secondLine, 17, 25);
            eccentricity = Double.parseDouble('.' +
                    secondLine.substring(26, 33).replace(' ', '0'));
            perigeeArgument = parseDouble(secondLine, 34, 42);
            meanAnomaly = parseDouble(secondLine, 43, 51);
            meanMotion = parseDouble(secondLine, 52, 63);
            revolutions = parseLong(secondLine.replace(' ', '0'), 63, 68);


        } catch (NumberFormatException exc) {
            throw new NumberFormatException("Input TLE string is wrong. "
                    + exc.getMessage());
        } catch (StringIndexOutOfBoundsException exc) {
            throw new StringIndexOutOfBoundsException("Input TLE string is wrong. "
                    + exc.getMessage());
        }
    }

    public Tle(final String firstLine, final String secondLine, final String name,
               long satelliteNumber, char category, int launchYear,
               int launchNumber, String launchPiece, int epochYear,
               double epochDay, double firstMeanMotionDerivative,
               double secondMeanMotionDerivative, double dragTerm,
               int elementNumber, double inclination, double ascendingNode,
               double eccentricity, double perigeeArgument, double meanAnomaly,
               double meanMotion, long revolutions) {

        this.firstLine = firstLine;
        this.secondLine = secondLine;
        this.name = name;
        this.satelliteNumber = satelliteNumber;
        this.category = category;
        this.launchYear = launchYear;
        this.launchNumber = launchNumber;
        this.launchPiece = launchPiece;
        this.epochYear = epochYear;
        this.epochDay = epochDay;
        this.firstMeanMotionDerivative = firstMeanMotionDerivative;
        this.secondMeanMotionDerivative = secondMeanMotionDerivative;
        this.dragTerm = dragTerm;
        this.elementNumber = elementNumber;
        this.inclination = inclination;
        this.ascendingNode = ascendingNode;
        this.eccentricity = eccentricity;
        this.perigeeArgument = perigeeArgument;
        this.meanAnomaly = meanAnomaly;
        this.meanMotion = meanMotion;
        this.revolutions = revolutions;
    }


    public String getFirstTleLine() {
        return firstLine;
    }

    public String getSecondTleLine() {
        return secondLine;
    }

    public String getSatelliteName() {
        return name;
    }

    public long getSatelliteNumber() {
        return satelliteNumber;
    }

    public char getCategory() {
        return category;
    }

    public int getLaunchYear() {
        return launchYear;
    }

    public int getLaunchNumber() {
        return launchNumber;
    }

    public String getLaunchPiece() {
        return launchPiece;
    }

    public int getEpochYear() {
        return epochYear;
    }

    public double getEpochDay() {
        return epochDay;
    }

    public double getFirstMeanMotionDerivative() {
        return firstMeanMotionDerivative;
    }

    public double getSecondMeanMotionDerivative() {
        return secondMeanMotionDerivative;
    }

    public double getDragTerm() {
        return dragTerm;
    }

    public int getEphemerisType() {
        return ephemerisType;
    }

    public int getElementNumber() {
        return elementNumber;
    }

    public double getInclination() {
        return inclination;
    }

    public double getAscendingNode() {
        return ascendingNode;
    }

    public double getEccentricity() {
        return eccentricity;
    }

    public double getPerigeeArgument() {
        return perigeeArgument;
    }

    public double getMeanAnomaly() {
        return meanAnomaly;
    }

    public double getMeanMotion() {
        return meanMotion;
    }

    public long getRevolutionsNumber() {
        return revolutions;
    }

    private int parseInt(final String str, int start, int finish) {
        return Integer.parseInt(str.substring(start, finish));
    }

    private double parseDouble(final String str, int start, int finish) {
        return Double.parseDouble(str.substring(start, finish));
    }

    private long parseLong(final String str, int start, int finish) {
        return Long.parseLong(str.substring(start, finish));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tle tle = (Tle) o;

        if (satelliteNumber != tle.satelliteNumber) return false;
        if (category != tle.category) return false;
        if (launchYear != tle.launchYear) return false;
        if (launchNumber != tle.launchNumber) return false;
        if (epochYear != tle.epochYear) return false;
        if (Double.compare(tle.epochDay, epochDay) != 0) return false;
        if (Double.compare(tle.firstMeanMotionDerivative, firstMeanMotionDerivative) != 0) return false;
        if (Double.compare(tle.secondMeanMotionDerivative, secondMeanMotionDerivative) != 0) return false;
        if (Double.compare(tle.dragTerm, dragTerm) != 0) return false;
        if (ephemerisType != tle.ephemerisType) return false;
        if (elementNumber != tle.elementNumber) return false;
        if (Double.compare(tle.inclination, inclination) != 0) return false;
        if (Double.compare(tle.ascendingNode, ascendingNode) != 0) return false;
        if (Double.compare(tle.eccentricity, eccentricity) != 0) return false;
        if (Double.compare(tle.perigeeArgument, perigeeArgument) != 0) return false;
        if (Double.compare(tle.meanAnomaly, meanAnomaly) != 0) return false;
        if (Double.compare(tle.meanMotion, meanMotion) != 0) return false;
        if (revolutions != tle.revolutions) return false;
        if (firstLine != null ? !firstLine.equals(tle.firstLine) : tle.firstLine != null) return false;
        if (secondLine != null ? !secondLine.equals(tle.secondLine) : tle.secondLine != null) return false;
        if (name != null ? !name.equals(tle.name) : tle.name != null) return false;
        return launchPiece != null ? launchPiece.equals(tle.launchPiece) : tle.launchPiece == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = firstLine != null ? firstLine.hashCode() : 0;
        result = 31 * result + (secondLine != null ? secondLine.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (satelliteNumber ^ (satelliteNumber >>> 32));
        result = 31 * result + (int) category;
        result = 31 * result + launchYear;
        result = 31 * result + launchNumber;
        result = 31 * result + (launchPiece != null ? launchPiece.hashCode() : 0);
        result = 31 * result + epochYear;
        temp = Double.doubleToLongBits(epochDay);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(firstMeanMotionDerivative);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(secondMeanMotionDerivative);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(dragTerm);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + ephemerisType;
        result = 31 * result + elementNumber;
        temp = Double.doubleToLongBits(inclination);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(ascendingNode);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(eccentricity);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(perigeeArgument);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(meanAnomaly);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(meanMotion);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (int) (revolutions ^ (revolutions >>> 32));
        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
