package org.jsattilates.spaceobject;

/**
 * Тип орбиты.
 */
public enum SatelliteOrbitType {
    LEO, MEO, GEO, HEO, UNDEFINED
}
