package org.jsattilates.spaceobject;

/**
 * Тип космического объекта.
 */
public enum SpaceObjectType {
    DEBRIS,         // Космический мусор
    WASTE_STAGE,    // Отработанная ступень
    SETTILATE       // Спутник
}
