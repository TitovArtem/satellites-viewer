package org.jsattilates.math;

import javax.vecmath.Vector3d;
import java.io.Serializable;

/**
 * Точка в трехмерном пространстве.
 */
public class Point3D implements Serializable, Cloneable {
    private static final long serialVersionUID = 1L;

    public double x, y, z;      // Координаты точки в пространстве

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Point3D() {
        this.x = this.y = this.z = 0.0;
    }

    public Point3D(Vector3d vector3d) {
        x = vector3d.x;
        y = vector3d.y;
        z = vector3d.z;
    }

    /**
     * Переводит трехмерные координаты в долготу и широту.
     * @param y2kDays количество дней с 1 января 2000 года
     * @return долгота и широта
     */
    public Coordinates toCoordinates(double y2kDays) {
        double lon = Math.atan2(y, x);
        double lat = Math.atan2(z, Math.sqrt(x * x + y * y));

        // рассчет смещения долготы по дате и времени
        double T = y2kDays / 36525.0;
        double rotDegree = ((280.46061837 + 360.98564736629 * y2kDays) +
                0.000387933 * T * T - T * T * T / 38710000.0) % 360.0;
        lon = lon - rotDegree * Math.PI / 180.0;
        double div = Math.floor(lon / (2 * Math.PI));
        lon = lon - div * 2 * Math.PI;
        if (lon > Math.PI)  lon = lon- 2.0*Math.PI;

        return new Coordinates(lon, lat);
    }

    public double getAltitude() {
        double earthRadius = 6378.1363;
        return Math.sqrt(x * x + y * y + z * z) - earthRadius;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point3D point3D = (Point3D) o;

        if (Double.compare(point3D.x, x) != 0) return false;
        if (Double.compare(point3D.y, y) != 0) return false;
        return Double.compare(point3D.z, z) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(z);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Point3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }
}
