package org.jsattilates.math;

import java.time.LocalDateTime;

/**
 * Класс для конвертации времени и дат.
 */
public class TimeUtils {

    /** Юлианская дата для 1 января 2001 **/
    public static final double Y2K_JD = 2451545.44362;

    public static double convertMinutesToDay(double minutes) {
        return minutes / 60.0 / 24.0;
    }

    public static double convertHoursToDay(double hours) {
        return hours / 24.0;
    }

    public static double convertDateTimeToDayOfYear(final LocalDateTime dateTime) {
        return dateTime.getDayOfYear() + convertHoursToDay(dateTime.getHour())
                + convertMinutesToDay(dateTime.getMinute());
    }

    public static double convertDateTimeToCurrentDay(final LocalDateTime dateTime) {
        return convertHoursToDay(dateTime.getHour()) + convertMinutesToDay(dateTime.getMinute());
    }

    public static double convertDateTimeToJulianDate(final LocalDateTime dateTime) {
        int year = dateTime.getYear();
        int month = dateTime.getDayOfMonth();
        int day = dateTime.getDayOfYear();
        int hour = dateTime.getHour();
        int minute = dateTime.getMinute();
        int second = dateTime.getSecond();

        double extra = (100.0 * year) + month - 190002.5;
        return (367.0 * year) -
                (Math.floor(7.0 * (year + Math.floor((month + 9.0) / 12.0)) / 4.0)) +
                Math.floor((275.0 * month) / 9.0) +
                day + ((hour + ((minute + (second / 60.0)) / 60.0)) / 24.0) +
                1721013.5 - ((0.5 * extra) / Math.abs(extra)) + 0.5;
    }

    public static double convertDateTimeToY2k(final LocalDateTime dateTime) {
        return convertDateTimeToJulianDate(dateTime) - Y2K_JD;
    }

    public static String convertDateTimeToString(final LocalDateTime dateTime) {
        return String.format("%s %d %d %d:%d:%d", dateTime.getMonth().toString(),
                dateTime.getDayOfMonth(), dateTime.getYear(),
                dateTime.getHour(), dateTime.getMinute(), dateTime.getSecond());
    }
}