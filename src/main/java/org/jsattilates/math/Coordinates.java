package org.jsattilates.math;


import java.io.Serializable;

/**
 * Георграфические координаты.
 */
public class Coordinates implements Serializable, Cloneable {

    private static final long serialVersionUID = 1L;

    /** Долгота. **/
    public double longitude;
    /** Широта. **/
    public double latitude;

    public Coordinates(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public Coordinates() {
        this.latitude = this.longitude = 0.0;
    }

    public Coordinates toRadians() {
        longitude = Math.toRadians(longitude);
        latitude = Math.toRadians(latitude);
        return this;
    }

    public Coordinates toDegrees() {
        longitude = Math.toDegrees(longitude);
        latitude = Math.toDegrees(latitude);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Coordinates that = (Coordinates) o;

        if (Double.compare(that.longitude, longitude) != 0) return false;
        return Double.compare(that.latitude, latitude) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(longitude);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Coordinates obj = (Coordinates)super.clone();
        return obj;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
