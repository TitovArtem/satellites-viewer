package org.jsattilates.gui.controller;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import org.jsattilates.db.DataBaseLoader;
import org.jsattilates.gui.SatellitesViewer;
import org.jsattilates.gui.model.Satellite;
import org.jsattilates.gui.model.SelectedSpaceObject;
import org.jsattilates.mapdrawer.CanvasMapDrawer;
import org.jsattilates.mapdrawer.MapDrawer;
import org.jsattilates.mapdrawer.MapProjection;
import org.jsattilates.mapdrawer.MercatorProjection;
import org.jsattilates.math.Coordinates;
import org.jsattilates.math.Point2D;
import org.jsattilates.math.Point3D;
import org.jsattilates.math.TimeUtils;
import org.jsattilates.physic.Propagator;
import org.jsattilates.physic.Sgp4Propagator;
import org.jsattilates.spaceobject.SpaceObject;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  Контролер для основного окна.
 */
public class SatellitesViewerController {
    @FXML
    private TableView<Satellite> spaceObjectsTable;
    @FXML
    private TableColumn<Satellite, Long> noradNumberColumn;
    @FXML
    private TableColumn<Satellite, String> spaceObjectNameColumn;
    @FXML
    private TableColumn<Satellite, String> spaceObjectTypeColumn;
    @FXML
    private TableColumn<Satellite, String> orbitTypeColumn;
    @FXML
    private TableColumn<Satellite, String> countryColumn;
    @FXML
    private TextField filterByNameTextField;
    @FXML
    private TextField filterByIdTextField;
    @FXML
    private Button addSpaceObjectBtn;

    @FXML
    private TableView<SelectedSpaceObject> selectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, Long> idSelectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, String> nameSelectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, Double> longSelectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, Double> latSelectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, Double> altSelectedObjsTv;
    @FXML
    private TableColumn<SelectedSpaceObject, Double> speedSelectedObjsTv;
    @FXML
    private Button deleteSelectedObjectBtn;

    @FXML
    private Label currentDateTimeLabel;

    @FXML
    private ImageView mapImageView;

    @FXML
    private Button  uploadTleButton;

    @FXML
    private Slider dateTimeSlider;


    private SatellitesViewer application;
    private Image map;                      // Карта в проекции Меркатора
    private Image trackedMap;               // Карта с трассами
    private Image currentMap;               // Текущее состояние карты
    private Propagator propagator;          // Объект для расчета трасс
    private MapProjection mapProjection;    // Объект для проецирования координат на карту
    private MapDrawer mapDrawer;            // Рендер трасс и меток объектов на карте
    private LocalDateTime currentDateTime;  // Текущая дата и время
    private double timeOffset;              // Смещение по времени в часах
    private Thread timeThread;
    private SelectedSpaceObject currentSelectedObject;

    private ObservableList<Satellite> spaceObjects = FXCollections.observableArrayList();
    private ObservableList<SelectedSpaceObject> selectedSpaceObjects =
            FXCollections.observableArrayList();


    public SatellitesViewerController() {
        timeThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true) {
                    try { Thread.sleep(1000); }
                    catch (InterruptedException exc) {
                        System.err.println(exc);
                    }

                    currentDateTime = LocalDateTime.now();
                    currentDateTime = currentDateTime.plusHours((long) timeOffset);

                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            currentDateTimeLabel.setText(TimeUtils.convertDateTimeToString(currentDateTime));
                            showSelectedObjects();
                        }
                    });
                }
            }
        });

        timeThread.start();
    }

    @FXML
    private void initialize() {
        noradNumberColumn.setCellValueFactory(cellData ->
                cellData.getValue().noradNumberProperty().asObject());
        spaceObjectNameColumn.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        spaceObjectTypeColumn.setCellValueFactory(cellData ->
                cellData.getValue().objectTypeProperty());
        orbitTypeColumn.setCellValueFactory(cellData -> cellData.getValue().orbitTypeProperty());
        countryColumn.setCellValueFactory(cellData -> cellData.getValue().countryProperty());

        idSelectedObjsTv.setCellValueFactory(cell ->
                cell.getValue().noradNumberProperty().asObject());
        nameSelectedObjsTv.setCellValueFactory(cell -> cell.getValue().nameProperty());
        longSelectedObjsTv.setCellValueFactory(cell ->
                cell.getValue().longitudeProperty().asObject());
        latSelectedObjsTv.setCellValueFactory(cell ->
                cell.getValue().latitudeProperty().asObject());
        altSelectedObjsTv.setCellValueFactory(cell ->
                cell.getValue().altitudeProperty().asObject());
        speedSelectedObjsTv.setCellValueFactory(cell -> cell.getValue().speedProperty().asObject());


        // TODO: invalid load
        final String mapImagePath = "mercator.jpg";
        map = new Image(mapImagePath);
        currentMap = new Image(mapImagePath);
        trackedMap = new Image(mapImagePath);
        mapDrawer = new CanvasMapDrawer();


        propagator = new Sgp4Propagator();
        mapProjection = new MercatorProjection((int)map.getWidth(), (int)map.getHeight());
        currentDateTime = LocalDateTime.now();

        mapImageView.setImage(map);
        mapImageView.setPreserveRatio(true);
        mapImageView.setSmooth(true);
        mapImageView.setCache(true);

        currentDateTimeLabel.setText(currentDateTime.toString());
        selectedObjsTv.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    currentSelectedObject = observable.getValue();
                });


        FilteredList<Satellite> filteredList = new FilteredList<>(spaceObjects, p -> true);
        filterByIdTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(satellite -> {
                if (newValue == null || newValue.isEmpty()) return true;
                String lowerCaseIdFilter = newValue.toLowerCase();
                String idString = satellite.getNoradNumber() + "";

                return idString.toLowerCase().indexOf(lowerCaseIdFilter) != -1;
            });
            SortedList<Satellite> sortedData = new SortedList<>(filteredList);
            sortedData.comparatorProperty().bind(spaceObjectsTable.comparatorProperty());
            spaceObjectsTable.setItems(sortedData);
        });

        filterByNameTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredList.setPredicate(satellite -> {
                if (newValue == null || newValue.isEmpty()) return true;
                String lowerCaseFilter = newValue.toLowerCase();
                return satellite.getName().toLowerCase().indexOf(lowerCaseFilter) != -1;
            });
            SortedList<Satellite> sortedData = new SortedList<>(filteredList);
            sortedData.comparatorProperty().bind(spaceObjectsTable.comparatorProperty());
            spaceObjectsTable.setItems(sortedData);
        });

        dateTimeSlider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                dateTimeSlider.setOnMouseReleased(new EventHandler<MouseEvent>() {
                    @Override
                    public void handle(MouseEvent event) {
                        timeOffset += newValue.doubleValue();
                        dateTimeSlider.setValue(0);
                        try {
                            timeThread.interrupt();
                            showSelectedObjects();
                            showSelectedObjectsTracks();
                            timeThread.start();
                        } catch (IllegalThreadStateException exc) {
                            System.out.println(exc);
                        }
                    }
                });
            }
        });


    }

    public Thread getTimeThread() {
        return timeThread;
    }


    public void setApplication(SatellitesViewer app) {
        application = app;
        spaceObjectsTable.setItems(spaceObjects);
    }


    public void handleAddObjectButtonAction(ActionEvent event) {
        Satellite selectedObject = spaceObjectsTable.getSelectionModel().getSelectedItem();
        if (selectedObject == null) return;
        SelectedSpaceObject object = new SelectedSpaceObject(selectedObject.getSpaceObject(),
                selectedObject.noradNumberProperty(), selectedObject.nameProperty());
        if (selectedSpaceObjects.contains(object)) return;
        selectedSpaceObjects.add(object);
        selectedObjsTv.setItems(selectedSpaceObjects);
        showSelectedObjectsTracks();
    }


    private void showSelectedObjects() {
        boolean isFirst = true;
        for (SelectedSpaceObject object: selectedSpaceObjects) {
            if (isFirst) {
                showSelectedSpaceObjectInfo(object, trackedMap);
                isFirst = false;
            }
            showSelectedSpaceObjectInfo(object, currentMap);
        }
    }


    private void showSelectedSpaceObjectInfo(SelectedSpaceObject object, Image map) {
        if (object == null) return;

        Point3D pos = propagator.getPosition(object.getSpaceObject(), currentDateTime);
        double offset = TimeUtils.convertDateTimeToY2k(currentDateTime);
        Coordinates coordinates[] = {pos.toCoordinates(offset)};
        coordinates[0].toDegrees();
        Point2D mark = mapProjection.project(coordinates)[0];

        CanvasMapDrawer drawer = (CanvasMapDrawer) mapDrawer;
        drawer.setObjectName(object.getName());
        if (currentSelectedObject != null && currentSelectedObject.equals(object)) {
            drawer.setObjectNameColor(Color.BLUE);
        } else {
            drawer.setObjectNameColor(Color.WHITE);
        }
        currentMap = drawer.drawPosition(map, mark);
        mapImageView.setImage(currentMap);

        object.setLongitude(coordinates[0].longitude);
        object.setLatitude(coordinates[0].latitude);
        object.setAltitude(pos.getAltitude());
        object.setSpeed(object.getSpaceObject().getVelocity(pos.getAltitude()));
    }


    private void showSelectedObjectsTracks() {
        boolean isFirst = true;
        for (SelectedSpaceObject obj: selectedSpaceObjects) {
            if (isFirst) {
                showSelectedSpaceObjectTrack(obj, map);
                isFirst = false;
            }
            showSelectedSpaceObjectTrack(obj, trackedMap);
        }
    }


    private void showSelectedSpaceObjectTrack(SelectedSpaceObject object, Image map) {
        Point2D[] track = getProjectedTrace(object.getSpaceObject());
        CanvasMapDrawer drawer = (CanvasMapDrawer) mapDrawer;
        drawer.setTrackColor(object.getTrackColor());
        trackedMap = drawer.drawTrack(map, track);
        mapImageView.setImage(trackedMap);
    }


    private Point2D[] getProjectedTrace(final SpaceObject object) {
        long period = (long) object.getPeriod();
        LocalDateTime upTime = currentDateTime.minusMinutes(period / 3);
        LocalDateTime toTime = currentDateTime.plusMinutes((long)(period * 1.5));

        Map trace = propagator.getTrace(object, upTime, toTime, 2);
        Object[] track = trace.values().toArray();
        Coordinates[] coordinates = new Coordinates[track.length];

        double shift = TimeUtils.convertDateTimeToY2k(upTime);
        double duration = TimeUtils.convertDateTimeToDayOfYear(toTime) -
                TimeUtils.convertDateTimeToDayOfYear(upTime);
        double step = duration / coordinates.length;

        for (int i = 0; i < coordinates.length; i++) {
            coordinates[i] = ((Point3D)track[i]).toCoordinates(shift);
            coordinates[i].toDegrees();
            shift += step;
        }

        return mapProjection.project(coordinates);
    }


    private String readFile(File file) throws IOException {
        StringBuilder sb =  new StringBuilder();

        BufferedReader fileReader = new BufferedReader(new FileReader(file));
        String s;
        while ((s = fileReader.readLine()) != null) {
            sb.append(s);
            sb.append("\n");
        }
        fileReader.close();

        return sb.toString();
    }


    public void addRecordInTable(SpaceObject obj) {
        boolean isInTable = false;
        Satellite sat = new Satellite(obj, obj.getTle().getSatelliteNumber(),
                obj.getTle().getSatelliteName(),
                obj.getOrbitType().name());
        for (int i = 0; i < spaceObjects.size() && !isInTable; i++) {
            if (spaceObjects.get(i).getName().compareTo(sat.getName()) == 0)
                isInTable = true;
        }
        if (!isInTable) {
            spaceObjects.add(sat);
            spaceObjectsTable.setItems(spaceObjects);
        }
    }

    public void fillTableFromBD() throws ClassNotFoundException, SQLException {
        List<SpaceObject> objs;
        try {
            objs = DataBaseLoader.getSpaceObjects();
        } catch (SQLException exc) {
            System.err.print(exc.getMessage());
            return;
        }

        List<String> objName = new ArrayList<>();
        for (int i = 0; i < objs.size(); i++) {
            if (objName.contains(objs.get(i).getTle().getSatelliteName())) {
                objs.remove(objs.get(i));
                i--;
            }
            else
                objName.add(objs.get(i).getTle().getSatelliteName());
        }
        for (SpaceObject obj : objs ) {
            spaceObjects.add(new Satellite(obj, obj.getTle().getRevolutionsNumber(),
                    obj.getTle().getSatelliteName(), obj.getOrbitType().name()));
        }
        spaceObjectsTable.setItems(spaceObjects);
    }


    private SpaceObject loadFromFile(File file) throws IOException {
        String tle = readFile(file);
        SpaceObject obj = new SpaceObject(tle);
        return obj;
    }


    public void handleDeleteSelectedObjectBtn(ActionEvent actionEvent) {
        SelectedSpaceObject object = selectedObjsTv.getSelectionModel().getSelectedItem();
        if (object == null) return;

        selectedSpaceObjects.remove(object);
        selectedObjsTv.setItems(selectedSpaceObjects);
        if (selectedSpaceObjects.size() > 0) showSelectedObjectsTracks();
        else mapImageView.setImage(map);
    }

    @FXML
    public void onClickMethod() throws IOException, SQLException, ClassNotFoundException {
        SpaceObject obj = null;
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Document");
        FileChooser.ExtensionFilter extFilter =
                new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            try {
                obj = loadFromFile(file);
                addRecordInTable(obj);
            } catch (IOException | StringIndexOutOfBoundsException | IllegalArgumentException e) {
                Alert message = new Alert(Alert.AlertType.ERROR);
                message.setTitle("ERROR!");
                message.setContentText("Impossible to read file. Incorrect data.");
                message.showAndWait();
                return;
            }
            try {
                DataBaseLoader.loadTleInDataBase(obj, currentDateTime);
            } catch (SQLException exc) {
                System.err.print(exc.getMessage());
            }
        }
    }

    @FXML
    private void getObjectsTleBtn() throws ClassNotFoundException, SQLException,
            ParserConfigurationException, SAXException, IOException {
        Satellite selectedObject = spaceObjectsTable.getSelectionModel().getSelectedItem();
        if (selectedObject == null) return;
        application.showTleDialog(selectedObject.getName());
    }
}