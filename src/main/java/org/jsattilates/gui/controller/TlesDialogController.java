package org.jsattilates.gui.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.jsattilates.db.DataBaseLoader;
import org.jsattilates.gui.model.TableTles;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by zannasapoval on 19.05.16.
 */
public class TlesDialogController {
    @FXML
    private TableView<TableTles> objectTlesTV;
    @FXML
    private TableColumn<TableTles, String> tleTlesTv;
    @FXML
    private TableColumn<TableTles, String> dataTlesTV;

    private Stage dialogStage;

    private ObservableList<TableTles> objectTles = FXCollections.observableArrayList();


    @FXML
    private void initialize() {
        tleTlesTv.setCellValueFactory(cell -> cell.getValue().tleProperty());
        dataTlesTV.setCellValueFactory(cell -> cell.getValue().dataProperty());

    }

    public void setDialogStage(Stage dialogStage) {
        this.dialogStage = dialogStage;
    }

    public void fillTable(String name) throws ClassNotFoundException, SQLException,
            ParserConfigurationException, SAXException, IOException {
        List<TableTles> tles = DataBaseLoader.getAllTleByName(name);
        objectTles.removeAll();
        objectTles.addAll(tles);
        objectTlesTV.setItems(objectTles);
    }
}
