package org.jsattilates.gui.model;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.paint.Color;
import org.jsattilates.spaceobject.SpaceObject;

import java.util.Random;

/**
 * Модель для описания отображаемых объектов.
 */
public class SelectedSpaceObject {

    private LongProperty noradNumber;
    private StringProperty name;
    private DoubleProperty longitude;
    private DoubleProperty latitude;
    private DoubleProperty altitude;
    private DoubleProperty speed;

    private SpaceObject spaceObject;
    private Color trackColor;

    public SelectedSpaceObject(SpaceObject spaceObject, LongProperty noradNumber,
                               StringProperty name) {

        if (spaceObject == null) {
            throw new NullPointerException("The given space object is null.");
        }

        trackColor = getRabdomColor();
        this.spaceObject = spaceObject;
        this.noradNumber = noradNumber;
        this.name = name;
        this.longitude = new SimpleDoubleProperty(0);
        this.latitude = new SimpleDoubleProperty(0);
        this.altitude = new SimpleDoubleProperty(0);
        this.speed = new SimpleDoubleProperty(0);
    }

    public SpaceObject getSpaceObject() {
        return spaceObject;
    }

    public void setSpaceObject(SpaceObject spaceObject) {
        if (spaceObject == null) {
            throw new NullPointerException("The given space object is null.");
        }
        this.spaceObject = spaceObject;
    }

    private Color getRabdomColor() {
        Random rand = new Random();
        float r = rand.nextFloat();
        float g = rand.nextFloat();
        float b = rand.nextFloat();
        return new Color(r, g, b, 1);
    }

    public Color getTrackColor() {
        return trackColor;
    }

    public void setTrackColor(Color trackColor) {
        if (trackColor == null) {
            throw new NullPointerException("The given color of tracks has null value.");
        }

        this.trackColor = trackColor;
    }

    public long getNoradNumber() {
        return noradNumber.get();
    }

    public LongProperty noradNumberProperty() {
        return noradNumber;
    }

    public void setNoradNumber(long noradNumber) {
        this.noradNumber.set(noradNumber);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getLongitude() {
        return longitude.get();
    }

    public DoubleProperty longitudeProperty() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude.set(longitude);
    }

    public double getLatitude() {
        return latitude.get();
    }

    public DoubleProperty latitudeProperty() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude.set(latitude);
    }

    public double getAltitude() {
        return altitude.get();
    }

    public DoubleProperty altitudeProperty() {
        return altitude;
    }

    public void setAltitude(double altitude) {
        this.altitude.set(altitude);
    }

    public double getSpeed() {
        return speed.get();
    }

    public DoubleProperty speedProperty() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed.set(speed);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SelectedSpaceObject object = (SelectedSpaceObject) o;

        return noradNumber != null ? noradNumber.equals(object.noradNumber) :
                object.noradNumber == null;

    }

    @Override
    public int hashCode() {
        return noradNumber != null ? noradNumber.hashCode() : 0;
    }
}
