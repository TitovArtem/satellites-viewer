package org.jsattilates.gui.model;

import javafx.beans.property.LongProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.jsattilates.spaceobject.SpaceObject;

/**
 * Модель для описания космического объекта в таблице.
 */
public class Satellite {

    private LongProperty noradNumber;
    private StringProperty name;
    private StringProperty objectType;
    private StringProperty orbitType;
    private StringProperty country;

    private SpaceObject spaceObject;

    public Satellite(SpaceObject spaceObject, long noradNumber, String name, String orbitType) {
        if (spaceObject == null) {
            throw new NullPointerException("The given space object is null.");
        }

        this.spaceObject = spaceObject;
        this.noradNumber = new SimpleLongProperty(noradNumber);
        this.name = new SimpleStringProperty(name);
        this.objectType = new SimpleStringProperty("");
        this.orbitType = new SimpleStringProperty(orbitType);
        this.country = new SimpleStringProperty("");
    }

    public SpaceObject getSpaceObject() {
        return spaceObject;
    }

    public void setSpaceObject(SpaceObject spaceObject) {
        if (spaceObject == null) {
            throw new NullPointerException("The given space object is null.");
        }
        this.spaceObject = spaceObject;
    }

    public long getNoradNumber() {
        return noradNumber.get();
    }

    public LongProperty noradNumberProperty() {
        return noradNumber;
    }

    public void setNoradNumber(long noradNumber) {
        this.noradNumber.set(noradNumber);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public String getObjectType() {
        return objectType.get();
    }

    public StringProperty objectTypeProperty() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType.set(objectType);
    }

    public String getOrbitType() {
        return orbitType.get();
    }

    public StringProperty orbitTypeProperty() {
        return orbitType;
    }

    public void setOrbitType(String orbitType) {
        this.orbitType.set(orbitType);
    }

    public String getCountry() {
        return country.get();
    }

    public StringProperty countryProperty() {
        return country;
    }

    public void setCountry(String country) {
        this.country.set(country);
    }
}
