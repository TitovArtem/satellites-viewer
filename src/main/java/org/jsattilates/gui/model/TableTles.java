package org.jsattilates.gui.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.jsattilates.spaceobject.Tle;

import java.time.LocalDateTime;

/**
 * todo
 */
public class TableTles {
    private StringProperty tle;
    private StringProperty data;

    public TableTles(String tle, String data) {
        this.tle = new SimpleStringProperty(tle);
        this.data = new SimpleStringProperty(data);
    }

    public String getTle() {
        return tle.get();
    }

    public StringProperty tleProperty() {
        return tle;
    }

    public void setTle(String tle) {
        this.tle.set(tle);
    }

    public String getData() {
        return data.get();
    }

    public StringProperty dataProperty() {
        return data;
    }

    public void setData(String data) {
        this.data.set(data);
    }
}
