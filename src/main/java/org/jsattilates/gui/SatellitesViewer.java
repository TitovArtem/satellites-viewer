package org.jsattilates.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.jsattilates.db.DataBase;
import org.jsattilates.db.DataBaseLoader;
import org.jsattilates.gui.controller.SatellitesViewerController;
import org.jsattilates.gui.controller.TlesDialogController;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;


public class SatellitesViewer extends Application {

    private SatellitesViewerController controller;
    private Stage primaryStage;

    public SatellitesViewer() {
    }

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        String fxmlFile = "/fxml/main.fxml";
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));
        this.primaryStage = stage;
        primaryStage.setTitle("Satellites Viewer");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

        DataBaseLoader.getInstance();
        DataBaseLoader.createDb();
        controller = loader.getController();
        controller.setApplication(this);
        controller.fillTableFromBD();

    }

    public void showTleDialog(String name) {
        try {
            // Загружаем fxml-файл и создаём новую сцену
            // для всплывающего диалогового окна.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(SatellitesViewer.class.getResource("/fxml/tlesDialog.fxml"));
            AnchorPane page = (AnchorPane) loader.load();


            // Создаём диалоговое окно Stage.
            Stage dialogStage = new Stage();
            dialogStage.setTitle("Objects TLEs");
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);

            // Передаём адресата в контроллер.
            TlesDialogController controller = loader.getController();
            controller.setDialogStage(dialogStage);
            controller.fillTable(name);

            // Отображаем диалоговое окно и ждём, пока пользователь его не закроет
            dialogStage.showAndWait();
        } catch (IOException | SAXException | SQLException
                | ParserConfigurationException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void stop() throws Exception {
        DataBaseLoader.disconnection();
        super.stop();
        controller.getTimeThread().stop();
    }
}
