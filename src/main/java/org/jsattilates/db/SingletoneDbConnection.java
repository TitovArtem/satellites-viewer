package org.jsattilates.db;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class SingletoneDbConnection
{
    private static SingletoneDbConnection singleInstance;
    private static Connection dbConnect;

    private SingletoneDbConnection() {}

    public static SingletoneDbConnection getInstance() throws SQLException {
        if(singleInstance == null) {
            synchronized (SingletoneDbConnection.class) {
                if(singleInstance == null) {
                    singleInstance = new SingletoneDbConnection();
                }
            }
        }
        return singleInstance;
    }

    public static Connection getConnInst() throws SQLException {
        if(dbConnect == null) {
            try {
                dbConnect  = DataBaseConnection.getDbConnection();
            } catch (SQLException e){
                throw new SQLException(e.getMessage());
            }
        }
        return dbConnect;
    }
}