package org.jsattilates.db;

import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Класс устанавливающий соединение с баззой данных.
 */
public class DataBaseConnection {

    /**
     * Установка соединения с базой данных.
     * @return
     * @throws SQLException
     */
    public static Connection getDbConnection() throws SQLException {
        Connection connection = null;
        ParserConfig config = null;
        try {
            config = new ParserConfig();
        } catch (SAXException | ParserConfigurationException | IOException exc) {
            throw new SQLException(exc.getMessage());
        }
        try {
            Class.forName("org.sqlite.JDBC");
            connection = DriverManager.getConnection("jdbc:sqlite:" + config.getDataBaseName(),
                    config.getLogin(), config.getPassword());
        } catch (SQLException | ClassNotFoundException exc) {
            throw new SQLException(exc.getMessage());
        }
        return connection;
    }
}