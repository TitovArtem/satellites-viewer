package org.jsattilates.db;

import org.jsattilates.gui.model.TableTles;
import org.jsattilates.spaceobject.SpaceObject;
import org.jsattilates.spaceobject.Tle;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Интерфейс для работы с базой данных.
 */
public final class DataBaseLoader {


    private static DataBaseLoader instance = null;
    private static DataBase dataBase = null;

    private DataBaseLoader() {}

    public static synchronized DataBaseLoader getInstance() throws SQLException {
        if (instance == null) {
            instance = new DataBaseLoader();
            dataBase = new DataBase();
        }
        return instance;
    }

    /**
     * Загражует объект в БД
     * @param obj объект для загрузки.
     * @param data дата TLE объекта.
     * @throws SQLException
     */
    public static void loadTleInDataBase(SpaceObject obj, LocalDateTime data) throws SQLException {
         dataBase.insertTlesTable(obj.getTle(), data.toString());
    }

    /**
     * Возвращает все значения из таблицы Tles в виде космических объектов.
     * @return Список объектов типа SpaceObject
     * @throws SQLException
     */
    public static List<SpaceObject> getSpaceObjects() throws SQLException {
        List<Tle> tles = dataBase.getAllTles();
        List<SpaceObject> objs = new ArrayList<>();
        for (Tle tle : tles) {
            objs.add(new SpaceObject(tle));
        }
        return  objs;
    }

    /**
     * Получает все Tle для космического объекта, находящихся в базе данных.
     * @param name имя космического объекта.
     * @return список Tle.
     * @throws SQLException
     */
    public static List<TableTles> getAllTleByName(String name) throws SQLException {
        List<TableTles> tles = dataBase.getAllTlesByName(name);
        return tles;
    }

    public static void createDb() throws SQLException {
        dataBase.createDbTles();
    }
    /**
     * Закрытие соединения с базой данных.
     * @throws SQLException
     */
    public static void disconnection() throws SQLException {
        dataBase.disconnection();
    }
}

