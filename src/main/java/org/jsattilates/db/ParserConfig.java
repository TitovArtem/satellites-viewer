package org.jsattilates.db;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

/**
 * Класс для получение информаци о базе данных из файла конфигурации.
 */

public class ParserConfig {
    private String password = null;
    private String login = null;
    private String dataBaseName;

    /**
     * Загрузка параметров базы данных из файла конфигурации.
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public ParserConfig() throws ParserConfigurationException, IOException, SAXException {
        File f = new File("src/main/resources/configuration.xml");
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(f);
        Element root = document.getDocumentElement();
        dataBaseName = root.getElementsByTagName("dataBaseName").item(0).getTextContent();
        login = root.getElementsByTagName("login").item(0).getTextContent();
        password = root.getElementsByTagName("password").item(0).getTextContent();
    }

    public String getPassword() {
        return password;
    }

    public String getLogin() {
        return login;
    }

    public String getDataBaseName() {
        return dataBaseName;
    }
}