package org.jsattilates.db;

import com.oracle.tools.packager.IOUtils;
import org.jsattilates.gui.model.TableTles;
import org.jsattilates.spaceobject.Tle;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс реализации запросов Sqlite.
 */
public class DataBase {


    private Connection connection = null;

    public DataBase() throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
    }

    /**
     * Создает таблицу Tles в базе данных.
     * @throws SQLException
     */
    public void createDbTles() throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute("CREATE TABLE if not exists 'Tles' (" +
                    "'id' INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "'firstLine' text, " +
                    "'secondLine' text, " +
                    "'name' text, " +
                    "'satelliteNumber' Long, " +
                    "'category' text, " +
                    "'launchYear' INTEGER, " +
                    "'launchNumber' INTEGER, " +
                    "'launchPiece' text, " +
                    "'epochYear' INTEGER, " +
                    "'epochDay' REAL, " +
                    "'firstMeanMotionDerivative' REAL, " +
                    "'secondMeanMotionDerivative' REAL, " +
                    "'dragTerm' REAL, " +
                    "'ephemerisType' INTEGER, " +
                    "'elementNumber' INTEGER, " +
                    "'inclination' REAL, " +
                    "'ascendingNode' REAL, " +
                    "'eccentricity' REAL, " +
                    "'perigeeArgument' REAL, " +
                    "'meanAnomaly' REAL, " +
                    "'meanMotion' DOUBLE, " +
                    "'revolutions' INTEGER," +
                    "'datetime' text);");
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        } finally {
            if (statement != null)
                statement.close();
        }
    }

    /**
     * Добавляет записи в таблицу Tles.
     * @param tle Tle добавляемого объекта.
     * @param data Дата Tle.
     * @throws SQLException
     */
    public void insertTlesTable(Tle tle, String data) throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        ResultSet resSet = null;
        int tle_id;
        try {
            statement = connection.createStatement();
            String commandStirngTle = "INSERT INTO 'Tles' ('firstLine', 'secondLine', " +
                    "'name', 'satelliteNumber', " +
                    "'category', 'launchYear', 'launchNumber', 'launchPiece', " +
                    "'epochYear', 'epochDay', " +
                    "'firstMeanMotionDerivative', 'secondMeanMotionDerivative', " +
                    "'dragTerm', 'ephemerisType', " +
                    "'elementNumber', 'inclination', 'ascendingNode', 'eccentricity', " +
                    "'perigeeArgument', " +
                    "'meanAnomaly', 'meanMotion', 'revolutions', 'datetime') " +
                    "VALUES (" + "'" + tle.getFirstTleLine() +
                    "'" + ", " + "'" + tle.getSecondTleLine() + "'" + ", " + "'" +
                    tle.getSatelliteName() + "'" + ", " + tle.getSatelliteNumber() + ", " +
                    "" + "'" + tle.getCategory() + "'" + ", " + tle.getLaunchYear() + ", " +
                    tle.getLaunchNumber() + ", " + "'" + tle.getLaunchPiece() + "'" + ", " +
                    tle.getEpochYear() + ", " + tle.getEpochDay() + ", " +
                    tle.getFirstMeanMotionDerivative() + ", " + tle.getSecondMeanMotionDerivative() +
                    ", " + tle.getDragTerm() + ", " + tle.getEphemerisType() +
                    ", " + tle.getElementNumber() + ", " + tle.getInclination() +
                    ", " + tle.getAscendingNode() + ", " + tle.getEccentricity() +
                    ", " + tle.getPerigeeArgument() + ", " + tle.getMeanAnomaly() +
                    ", " + tle.getMeanMotion() + ", " + (Long) tle.getRevolutionsNumber() +
                    ", " + " '" + data + "' " + ");";
            System.out.println(commandStirngTle);
            statement.execute(commandStirngTle);
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        } finally {
            if (statement != null)
                statement.close();
        }
    }

    /**
     * Получение всех записей из таблицы Tles.
     * @return Список записей в форме объектов Tle.
     * @throws SQLException
     */
    public List<Tle> getAllTles() throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        List<Tle> tles = new ArrayList<>();
        ResultSet resSet = null;
        try {
            statement = connection.createStatement();
            resSet = statement.executeQuery("Select * FROM 'Tles';");
            while (resSet.next()) {
                tles.add(new Tle(resSet.getString("firstLine"), resSet.getString("secondLine"),
                        resSet.getString("name"),
                        resSet.getLong("satelliteNumber"), resSet.getString("category").charAt(0),
                        resSet.getInt("launchYear"), resSet.getInt("launchNumber"),
                        resSet.getString("launchPiece"), resSet.getInt("epochYear"),
                        resSet.getDouble("epochDay"), resSet.getDouble("firstMeanMotionDerivative"),
                        resSet.getDouble("secondMeanMotionDerivative"), resSet.getDouble("dragTerm"),
                        resSet.getInt("elementNumber"), resSet.getDouble("inclination"),
                        resSet.getDouble("ascendingNode"), resSet.getDouble("eccentricity"),
                        resSet.getDouble("perigeeArgument"), resSet.getDouble("meanAnomaly"),
                        resSet.getDouble("meanMotion"), resSet.getLong("revolutions")));
            }
            resSet.close();
        } catch (SQLException exc) {
            throw exc;//new SQLException(exc.getMessage());
        } finally {
            if (resSet != null)
                resSet.close();
            if (statement != null)
                statement.close();
        }
        return tles;
    }

    /**
     * Получение записей из таблицы Tles для спутника, заданного именем.
     * @param name имя спутника.
     * @return Список Tle для объекта.
     * @throws SQLException
     */
    public List<TableTles> getAllTlesByName(String name) throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        List<TableTles> results = new ArrayList<>();
        ResultSet resSet = null;
        try {
            statement = connection.createStatement();
            resSet = statement.executeQuery("Select * FROM 'Tles' " +
                    "WHERE name = " + "'" + name + "'" + ";");
            while (resSet.next()) {
                String tle = resSet.getString("firstLine") + " " + resSet.getString("secondLine");
                results.add(new TableTles(tle, resSet.getString("datetime")));
            }
            resSet.close();
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        } finally {
            if (resSet != null)
                resSet.close();
            if (statement != null)
                statement.close();
        }
        return results;
    }


    /**
     * Получение даты Tle по id.
     * @param id_tle id Tle.
     * @return  Дата Tle.
     * @throws SQLException
     */
    public String getDataFromTleRecords(int id_tle) throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        ResultSet resSet = null;
        String result;
        try {
            statement = connection.createStatement();
            resSet = statement.executeQuery("SELECT * FROM Tles " +
                "WHERE id = " + id_tle);
            result = resSet.getString("datetime");
            resSet.close();
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        } finally {
            if (resSet != null)
                resSet.close();
            if (statement != null)
                statement.close();
        }
        return result;
    }

    /**
     * Получение записи из таблицы Tle.
     * @param id_tle id записи.
     * @return ResultSet для данного id.
     * @throws SQLException
     */
    public ResultSet getStringFromTle(int id_tle) throws SQLException {
        connection = SingletoneDbConnection.getInstance().getConnInst();
        Statement statement = null;
        ResultSet resSet = null;
        try {
            statement = connection.createStatement();
            resSet = statement.executeQuery("SELECT * FROM Tles " +
                "WHERE id = " + id_tle);
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        } finally {
            if (resSet != null)
                resSet.close();
            if (resSet != null)
                resSet.close();
            if (statement != null)
                statement.close();
        }
        return resSet;
    }

    /**
     * Закрытие соединения с базой данных.
     * @throws SQLException
     */
    public void disconnection() throws SQLException {
        try {
            if (connection != null)
                connection.close();
        } catch (SQLException exc) {
            throw new SQLException(exc.getMessage());
        }
    }

}
