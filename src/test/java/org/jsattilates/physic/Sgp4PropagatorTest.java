package org.jsattilates.physic;

import jdk.internal.org.objectweb.asm.TypeReference;
import org.jsattilates.math.Point3D;
import org.jsattilates.spaceobject.SpaceObject;
import org.junit.Before;
import org.junit.Test;

import javax.vecmath.Point3d;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.*;

import static org.junit.Assert.*;

/**
 * Тесты для {@link org.jsattilates.physic.Sgp4Propagator}.
 */
public class Sgp4PropagatorTest {

    private SpaceObject spaceObject;
    private Sgp4Propagator propagator;

    @Before
    public void setUp() throws Exception {
        spaceObject = new SpaceObject("GOES 13\n" +
                "1 29155U 06018A   16122.59737619 -.00000259  00000-0  00000+0 0  9992\n" +
                "2 29155   0.2908 269.5726 0006791 153.0212 297.4853  1.00264065 36435");
        propagator = new Sgp4Propagator();
    }

    @Test
    public void testGetSinglePosition() throws Exception {
        Point3D pos = propagator.getPosition(spaceObject,
                LocalDateTime.of(2016, Month.MAY, 3, 12, 12, 12));
        Point3D truePos = new Point3D(36450, -21201, 199);
        assertEquals(pos.x, truePos.x, 100);
        assertEquals(pos.y, truePos.y, 100);
        assertEquals(pos.z, truePos.z, 1);
    }

    @Test
    public void testGetTrace() throws Exception {
        Map<LocalDateTime, Point3D> trace = propagator.getTrace(spaceObject,
                                    LocalDateTime.of(2016, Month.MAY, 9, 14, 38),
                                    LocalDateTime.of(2016, Month.MAY, 9, 23, 38),
                                    100);

        Collection<Point3D> truePoses = new ArrayList<Point3D>();

        truePoses.add(new Point3D(41189, 8944, 212));
        truePoses.add(new Point3D(37448, 19333, 193));
        truePoses.add(new Point3D(31138, 28395, 161));
        truePoses.add(new Point3D(22690, 35509, 118));
        truePoses.add(new Point3D(12685, 40185, 67));
        truePoses.add(new Point3D(1809, 42103, 11));
        truePoses.add(new Point3D(-9190, 41131, -45));
        truePoses.add(new Point3D(-19560, 37338, -99));
        truePoses.add(new Point3D(-28588, 30983, -145));
        truePoses.add(new Point3D(-35656, 22505, -182));

        Collection<LocalDateTime> trueDateTime = new ArrayList<LocalDateTime>();

        for (int i = 14; i < 24; i++) {
            trueDateTime.add(LocalDateTime.of(2016, Month.MAY, 9, i, 38));
        }

        Map<LocalDateTime, Point3D> trueTrace = new LinkedHashMap<LocalDateTime, Point3D>();

        for (int i = 0; i < truePoses.size(); i++){
            trueTrace.put((LocalDateTime) trueDateTime.toArray()[i], (Point3D) truePoses.toArray()[i]);
        }

        for (int i = 0; i < trueTrace.size(); i++ ) {
            Point3D pos = (Point3D) trace.values().toArray()[i];
            Point3D truePos = (Point3D) trueTrace.values().toArray()[i];
            assertEquals(pos.x, truePos.x, 1000);
            assertEquals(pos.y, truePos.y, 1000);
            assertEquals(pos.z, truePos.z, 100);
        }
    }
}