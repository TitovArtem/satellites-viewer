package org.jsattilates.spaceobject;

import org.junit.Before;
import org.junit.Test;

import java.text.NumberFormat;

import static org.junit.Assert.*;

/**
 * Тест для {@link org.jsattilates.spaceobject.Tle}.
 */
public class TleTest {

    private Tle tle;

    @Before
    public void setUp() throws Exception {
        tle = new Tle("GIOVE-A\n" +
                "1 28922U 05051A   16118.24207793  .00000027  00000-0  00000+0 0  9993\n" +
                "2 28922  56.8990  90.8984 0005858   9.3227 350.6645  1.69417391 64032");
    }

    @Test(expected = StringIndexOutOfBoundsException.class)
    public void testInvalidTleLength() throws Exception {
        new Tle("GIOVE-A\n" +
                "1 28922U 05051A \n" +
                "2 28922  56.8990  90.8984 0005858   9.3227 350.6645  1.69417391 64032");
    }

    @Test(expected = NumberFormatException.class)
    public void testInvalidFirstLineFormat() throws NumberFormatException {
        new Tle("GIOVE-A\n" +
                "1 28922U 05051A   16118.24ERROR_HERE207793  .00000027  00000-0  00000+0 0  9993\n" +
                "2 28922  56.8990  90.8984 0005858   9.3227 350.6645  1.69417391 64032");
    }

    @Test(expected = NumberFormatException.class)
    public void testInvalidSecondLineFormat() throws NumberFormatException {
        new Tle("GIOVE-A\n" +
                "1 28922U 05051A   16118.24207793  .00000027  00000-0  00000+0 0  9993\n" +
                "2 28922  56.8990  90.8984 0005858   9.3227 350.6645  1.69ERROR_HERE417391 64032");
    }

    @Test
    public void testSatelliteNumber() throws Exception {
        assertEquals(tle.getSatelliteNumber(), 28922);
    }

    @Test
    public void testSatelliteName() throws Exception {
        assertEquals(tle.getSatelliteName(), "GIOVE-A");
    }

    @Test
    public void testSatelliteCathegory() throws Exception {
        assertEquals(tle.getCategory(), 'U');
    }

    @Test
    public void testSatelliteLaunch() throws Exception {
        assertEquals(tle.getLaunchYear(), 5);
        assertEquals(tle.getLaunchNumber(), 51);
        assertEquals(tle.getLaunchPiece(), "A");
    }

    @Test
    public void testSatelliteEpoch() throws Exception {
        assertEquals(tle.getEpochYear(), 16);
        assertEquals(tle.getEpochDay(), 118.24207793, 0);
    }

    @Test
    public void testSatelliteElementNumber() throws Exception {
        assertEquals(tle.getElementNumber(), 999);
    }

    @Test
    public void testSatelliteMeanMotionDerivative() throws Exception {
        assertEquals(tle.getFirstMeanMotionDerivative(), 2.7E-7, 0);
        assertEquals(tle.getSecondMeanMotionDerivative(), 0, 0);
    }

    @Test
    public void testSatelliteInclination() throws Exception {
        assertEquals(tle.getInclination(), 56.899, 0);
    }

    @Test
    public void testSatelliteAscendingNode() throws Exception {
        assertEquals(tle.getAscendingNode(), 90.8984, 0);
    }

    @Test
    public void testSatelliteEccentricity() throws Exception {
        assertEquals(tle.getEccentricity(), 5.858E-4, 0);
    }

    @Test
    public void testSatellitePerigeeArgument() throws Exception {
        assertEquals(tle.getPerigeeArgument(), 9.3227, 0);
    }

    @Test
    public void testSatelliteMeanAnomaly() throws Exception {
        assertEquals(tle.getMeanAnomaly(), 350.6645, 0);
    }

    @Test
    public void testSatelliteMeanMotion() throws Exception {
        assertEquals(tle.getMeanMotion(), 1.69417391, 0);
    }

    @Test
    public void testSatelliteRevolution() throws Exception {
        assertEquals(tle.getRevolutionsNumber(), 6403);
    }
}