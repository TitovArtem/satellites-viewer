package org.jsattilates.spaceobject;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by artembars on 11.05.16.
 */
public class SpaceObjectTest {
    SpaceObject geo1 = new SpaceObject("0 NATO 2A\n" +
            "1 04353U 70021A   16129.54894894 -.00000118  00000-0  00000+0 0  9995\n" +
            "2 04353   8.7083 313.9022 0003563 202.7042 161.4918  1.00281863 88877");
    SpaceObject geo2 = new SpaceObject("0 INTELSAT 3-F6\n" +
            "1 04297U 70003A   16132.16688255  .00000048  00000-0  00000+0 0  9997\n" +
            "2 04297   7.6492 306.9552 0010361 343.0211 232.7254  0.99155337140537");
    SpaceObject meo1 = new SpaceObject("0 NTS 2\n" +
            "1 10091U 77053A   16130.88614152 -.00000064 +00000-0 +00000-0 0  9990\n" +
            "2 10091 064.3518 035.8938 0060653 227.9463 047.5635 02.00447634284700");
    SpaceObject meo2 = new SpaceObject("0 OPS 9794 (NAVSTAR 8)\n" +
            "1 14189U 83072A   16131.25610633  .00000030  00000-0  00000+0 0  9993\n" +
            "2 14189  62.1362 175.7525 0150941  34.8209 331.8093  1.91719143156064");
    SpaceObject leo1 = new SpaceObject("0 RADIATION SAT (5E 1)\n" +
            "1 00671U 63038C   16131.61121036  .00000042  00000-0  55207-4 0  9995\n" +
            "2 00671  90.0813 321.7970 0042175 116.9557 311.6337 13.44751444579708");
    SpaceObject leo2 = new SpaceObject("0 TIROS 4\n" +
            "1 00226U 62002A   16131.21008255 -.00000093  00000-0  21702-4 0  9994\n" +
            "2 00226  48.2952 122.1912 0078303 193.5098 166.3705 14.45537744855332");
    SpaceObject heo1 = new SpaceObject("0 RELAY 1 (A-15)\n" +
            "1 00503U 62068A   16132.17503730 -.00000032  00000-0  00000+0 0  9994\n" +
            "2 00503  47.5044 105.0749 2842897  65.7184  25.4576  7.78150436518533");
    SpaceObject heo2 = new SpaceObject("0 MOLNIYA 3-3\n" +
            "1 08425U 75105A   16130.18320763 -.00000363  00000-0 -64149-3 0  9999\n" +
            "2 08425  61.1641 190.1478 7411654 261.1918  16.7147  2.00666242296647");


    @Test
    public void testOrbitType() throws Exception {
        assertEquals(geo1.getOrbitType(), SatelliteOrbitType.GEO);
        assertEquals(geo2.getOrbitType(), SatelliteOrbitType.GEO);
        assertEquals(meo1.getOrbitType(), SatelliteOrbitType.MEO);
        assertEquals(meo2.getOrbitType(), SatelliteOrbitType.MEO);
        assertEquals(leo1.getOrbitType(), SatelliteOrbitType.LEO);
        assertEquals(leo2.getOrbitType(), SatelliteOrbitType.LEO);
        assertEquals(heo1.getOrbitType(), SatelliteOrbitType.HEO);
        assertEquals(heo2.getOrbitType(), SatelliteOrbitType.HEO);
    }

}